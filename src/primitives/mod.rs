pub mod edges;
pub mod no_edges;

pub trait Shape {
    fn area(&self) -> f32;
}

pub struct Point {
    pub x: f32,
    pub y: f32,
}

impl Point {
    pub fn new(x: f32, y: f32) -> Point {
        Point {x, y}
    }

    pub fn scale(&mut self, scalar: f32) {
        self.x *= scalar;
        self.y *= scalar;
    }
}
