use super::Shape;

pub struct Rectangle {
    pub width: f32,
    pub height: f32,
}

impl Shape for Rectangle {
    fn area(&self) -> f32 {
        self.width * self.height
    }
}

impl Rectangle {
    pub fn new(width: f32, height: f32) -> Self {
        Self {width, height}
    }
}