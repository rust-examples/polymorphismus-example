use super::Shape;

pub struct Circle {
    pub radius: f32,
}

impl Shape for Circle {
    fn area(&self) -> f32 {
        self.radius * 2.0 * std::f32::consts::PI
    }
}

impl Circle {
    pub fn new(r: f32) -> Self {
        Self {radius: r}
    }
}