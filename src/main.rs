// Wie würde in python eine klasse aussehen
/*
class Point:
    def __init__(self,x, y):
        self.x = x
        self.y = y

        class Point:
    def __init__(self,x, y):
        if not isInstance(x, float):
            raise TypeError("X muss ein float sein")
        if not isInstance(y, float):
            raise TypeError("x muss ein float sein")
        if  x< < 0.0 or y < 0.0:
            raise ValueError("x und y müssen beide positiv sein")
        self.x = x
        self.y = y
*/

mod test {
    pub fn test() {
        println!("test");
    }
}

mod primitives;
use primitives::{Shape, edges::Rectangle};
use primitives::no_edges::Circle;


// Hier sagen wir, hey akzeptiere
// jeden Typ, der eine `Eigenschaft` implementiert
// namens "Shape" implementiert. z.B. jedes Objekt, das
// laufen kann (aus der Hund-Mensch-Analogie).
fn print_area(shape: &dyn Shape) {
    println!("dyn = {}", shape.area());
}

// Oder wir können auch eine generische Funktion erstellen.
// Dies ist ähnlich wie `print_area`, kann aber
// hat aber den Vorteil der statischen Typüberprüfung,
// da es sich um eine generische Funktion handelt, so dass zwei Versionen der
// gleichen Funktion mit relevanten
// absoluten/konkreten Typen erzeugt. (bin mir aber nicht sicher)
fn print_area_generic<T: Shape> (shape: &T) {
    println!("generic = {}", shape.area());
}

fn main() {
    use primitives::Point;
    let mut pt = Point::new(1.0 / 3.9, 0.0);
    let circle = Circle::new(30.0);
    let rectangle = Rectangle::new(30.0, 50.0);

    {
        let mut pt = Point::new(4.0, 4.0);
        pt.scale( 2.0);
        println!("Shadow x: {} und y: {}", pt.x, pt.y);
    }
    pt.scale( 2.0);
    println!("x: {} und y: {}", pt.x, pt.y);

    test::test();

    // So möglich
    println!("circle area: {}", circle.area());
    println!("rectangle area: {}", rectangle.area());

    // dyn
    print_area(&circle);
    print_area(&rectangle);
    // Oder über generic
    print_area_generic(&circle);
    print_area_generic(&rectangle);
}





